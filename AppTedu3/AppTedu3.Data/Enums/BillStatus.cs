﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AppTedu3.Data.Enums
{
    public enum BillStatus
    {
        [Description("New Bill")]
        New,
        [Description("In Progress")]
        InProgress,
        [Description("Returned")]
        Returned,
        [Description("Cancelled")]
        Cancelled,
        [Description("Completed")]
        Completed
    }
}
