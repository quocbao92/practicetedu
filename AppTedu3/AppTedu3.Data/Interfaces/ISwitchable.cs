﻿using AppTedu3.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTedu3.Data.Interfaces
{
    public interface ISwitchable
    {
        Status Status { get; set; }
    }
}
