﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppTedu3.Data.Interfaces
{
    public interface IHasOwner<T>
    {
        T OwnerId { get; set; }
    }
}
