﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppTedu3.Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {

        void Commit();
    }
}
