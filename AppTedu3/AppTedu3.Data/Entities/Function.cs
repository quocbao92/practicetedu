﻿using AppTedu3.Data.Enums;
using AppTedu3.Data.Interfaces;
using AppTedu3.Infrastructure.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AppTedu3.Data.Entities
{
    [Table("Functions")]
    public class Function : DomainEntity<string>, ISwitchable, ISortable
    {
        public Function()
        {

        }

        public Function(string name, string url, string parentId, string iconCss, int sortOrder)
        {

            this.Name = name;
            this.URL = url;
            this.ParentId = parentId;
            this.IconCss = iconCss;
            this.SortOrder = sortOrder;
            this.Status = Status.Active;
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string URL { get; set; }

       
        [StringLength(128)]
        public string ParentId { get; set; }

        public string IconCss { get; set; }



        public int SortOrder { get; set; }
        public Status Status { get; set; }
    }
}
