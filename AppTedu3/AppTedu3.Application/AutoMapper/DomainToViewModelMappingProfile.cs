﻿using AppTedu3.Application.ViewModels;
using AppTedu3.Data.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTedu3.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            //CreateMap<ProductCategory, ProductCategoryViewModel>()
            //    .ForMember(d => d.DateCreated, opt => opt.MapFrom(src => src.DateCreated))
            //    .ForMember(d => d.DateModified, opt => opt.MapFrom(src => src.DateModified))
            //    .ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(d => d.Description, opt => opt.MapFrom(src => src.Description))
            //    .ForMember(d => d.ParentId, opt => opt.MapFrom(src => src.ParentId))
            //    .ForMember(d => d.HomeOrder, opt => opt.MapFrom(src => src.HomeOrder))
            //    .ForMember(d => d.Image, opt => opt.MapFrom(src => src.Image))
            //    .ForMember(d => d.HomeFlag, opt => opt.MapFrom(src => src.HomeFlag))
            //    .ForMember(d => d.Status, opt => opt.MapFrom(src => src.Status))
            //    .ForMember(d => d.SeoPageTitle, opt => opt.MapFrom(src => src.SeoPageTitle))
            //    .ForMember(d => d.SeoAlias, opt => opt.MapFrom(src => src.SeoAlias))
            //    .ForMember(d => d.SeoKeywords, opt => opt.MapFrom(src => src.SeoKeywords))
            //    .ForMember(d => d.SeoDescription, opt => opt.MapFrom(src => src.SeoDescription))
            //    .ForMember(d => d.SortOrder, opt => opt.MapFrom(src => src.SortOrder))
            //    .ForMember(d => d.Products, opt => opt.MapFrom(src => src.Products));
            CreateMap<ProductCategory, ProductCategoryViewModel>();
        }
    }
}
