﻿using AppTedu3.Data.Entities;
using AppTedu3.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppTedu3.Data.IRepositories
{
    public interface IProductCategoryRepository : IRepository<ProductCategory,int>
    {
        List<ProductCategory> GetByAlias(string alias);
    }
}
